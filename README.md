# dai-trainer-example

**EVALUACIÓN TECNOLÓGICA 2021**

_Consideraciones Generales:_

    - **Tiempo:** 3 horas
    - **Tecnologías a utilizar:** spring boot 2 [Spring 5], Thymeleaf, Hibernate, JPA, Oracle, JQuery, AJAX, DataTables.
    - **Esquema de la base de datos:**

| CAT_ESTADO |
| ------ |
| ID                number(38,0)PK |
| DESCRIPCION       varchar2(50) |
| CODIGO            varchar2(10) |
                                            

| CLIENTE |
| ------ |
| ID                number(38,0)PK |
| NOMBRES           varchar2(250) |
| APELLIDOS         varchar2(250) |
| TELEFONO          varchar2(8) |
| EMAIL             varchar2(100) |
| DIRECCION         varchar2(300) |
| FECHA_CREA        date |

| PRODUCTO |
| ------ |
| ID                number(38,0)PK |
| DESCRIPCION       varchar2(200) |
| PRECIO            number(16,2) |

| ORDEN |
| ------ |
| ID                number(38,0)PK |
| CIENTE_ID         number(38,0)FK |
| PRODUCTO_ID       number(38,0)FK |
| CAT_ESTADO_ID     number(38,0)FK |
| CANTIDAD          number(38,0) |
| MONTO_TOTAL       number(16,2) |
| USUARIO_CREA      varchar2(100) |
| FECHA_CREA        date |

_Enunciados:_

* [25%] Desarrolle una consulta donde muestre los campos: CODIGO_ORDEN (ID de la Orden), NOMBRES, APELLIDOS, DIRECCION, PRODUCTO, PRECIO_UNITARIO, CANTIDAD, MONTO_TOTAL y ESTADO_ORDEN que sus montos totales estén por encima de $100. 

  Para los campos PRECIO_UNITARIO y MONTO_TOTAL cree una función que permita mostrarlo en el formato _99,999.99_ con la etiqueta de moneda "USD". Nombrar la función FORMAT_CURRENCY.

  Deberá además mostrar ordenados los datos por el campo CODIGO_ORDEN. La tabla resultante debe quedar como se muestra a continuación:

| CODIGO_ORDEN | NOMBRES | APELLIDOS | DIRECCION | PRODUCTO | PRECIO_UNITARIO | CANTIDAD | MONTO_TOTAL | ESTADO_ORDEN |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| 10 | Jorge Alberto | Solorzano Guardado | San Salvador | TV Sony Smart 55 4k OLed | USD 650.55 | 4 | 2,602.20 | REGISTRADA |

* [25%] Utilizando la consulta anterior, desarrolle una pantalla que muestre los datos en una tabla DataTables con paginación de 5 registros.

* [40%] Desarrolle un formulario para ingreso de Ordenes, en el cual debe mostrar un combo para seleccionar un cliente y un combo para seleccionar un producto.
  - Al guardar debe de calcular el monto total multiplicando la cantidad de productos por el precio del producto y además debe de guardar el registro con el estado "REGISTRADA", el campo ESTADO_ORDEN no debe mostrarse en este formulario.
  - Al guardar debe llenar los campos FECHA_CREA con la fecha y hora que se ejecuta la operación, y el campo USUARIO_CREA con un String que tenga su nombre y apellido.
  - Utilice sercuencias.

* [10%] Implemente un botón "DESPACHAR" que por medio de una petición AJAX, al seleccionar de la tabla un registro y hacer clic en él, permita cambiar el estado "REGISTRADA" a "DESPACHADA" a la orden.
